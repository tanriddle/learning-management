<?php
/**
 * @var $args
 */

if (empty($args)) $args = array();
if (!empty($_GET['search'])) {
	$args['s'] = sanitize_text_field($_GET['search']);
}

$default_args = array(
	'post_type'      => 'stm-courses',
	'posts_per_page' => STM_LMS_Options::get_option('courses_per_page', get_option('posts_per_page')),
);

$args['image_d'] = !empty($args['image_d']) ? $args['image_d'] : 'img-300-225';

$args = wp_parse_args($args, $default_args);
$q = new WP_Query($args);

$per_row = (!empty($args['per_row'])) ? $args['per_row'] : STM_LMS_Options::get_option('courses_per_row', 3);
$class = (!empty($args['class'])) ? $args['class'] : '';

if ($q->have_posts()):
	$course_view = 'stm_lms_courses__grid_' . STM_LMS_Options::get_option('course_card_view', 'center');

	stm_lms_register_style('courses');

	if (empty($args['isAjax'])): ?>
        <div class="stm_lms_courses__grid stm_lms_courses__grid_<?php echo esc_attr($per_row . ' ' . $course_view); ?> <?php echo esc_attr($class); ?>"
        data-pages="<?php echo ceil($q->found_posts / $args['posts_per_page']); ?>">
	<?php endif; ?>
	<?php while ($q->have_posts()):
	$q->the_post();
	$id = get_the_ID();
	global $post;
	$post_status = STM_LMS_Course::get_post_status($id);
	$terms = stm_lms_get_terms_array($id, 'stm_lms_course_taxonomy', false, true);
	$price = get_post_meta($id, 'price', true);
	$sale_price = STM_LMS_Course::get_sale_price($id);

	if (empty($price) and !empty($sale_price)) {
		$price = $sale_price;
		$sale_price = '';
	}

	$has_sale_price = !empty($sale_price) ? 'has-sale' : 'no-sale';

	$author_id = $post->post_author; ?>

    <div class="stm_lms_courses__single stm_lms_courses__single_animation <?php echo esc_attr($has_sale_price); ?>">

        <div class="stm_lms_courses__single__inner">

            <div class="stm_lms_courses__single--image">

				<?php if (!empty($post_status)): ?>
                    <div class="stm_lms_post_status heading_font <?php echo sanitize_text_field($post_status['status']); ?>">
						<?php echo sanitize_text_field($post_status['label']); ?>
                    </div>
				<?php endif; ?>

                <a href="<?php the_permalink(); ?>">
                    <div>
						<?php
						if (function_exists('stm_get_VC_img')) {
							echo html_entity_decode(stm_get_VC_img(get_post_thumbnail_id(), '272x161'));
						} else {
							the_post_thumbnail($args['image_d']);
						}
						?>
                    </div>
                </a>
            </div>

            <div class="stm_lms_courses__single--inner">

				<?php if (!empty($terms)):
					$terms = array_splice($terms, 0, 1);
					?>
                    <div class="stm_lms_courses__single--terms">
                        <div class="stm_lms_courses__single--term">
							<?php echo sanitize_text_field(implode('', $terms)); ?> >
                        </div>
                    </div>
				<?php endif; ?>

                <div class="stm_lms_courses__single--title">
                    <a href="<?php the_permalink(); ?>">
                        <h5><?php the_title(); ?></h5>
                    </a>
                </div>

                <div class="stm_lms_courses__single--meta">
					<?php STM_LMS_Templates::show_lms_template('courses/parts/rating', compact('id')); ?>

					<?php STM_LMS_Templates::show_lms_template('global/price', compact('price', 'sale_price')); ?>

                </div>


            </div>

			<?php STM_LMS_Templates::show_lms_template('courses/parts/course_info',
				array_merge(array(
					'post_id' => $id,
				), compact('post_status', 'sale_price', 'price', 'author_id', 'id'))
			); ?>
        </div>

    </div>

<?php endwhile; ?>
	<?php if (empty($args['isAjax'])): ?>
    </div>
<?php endif; ?>
<?php else: ?>
    <h4><?php esc_html_e('No courses found.', 'masterstudy-lms-learning-management-system'); ?></h4>
<?php endif;
wp_reset_postdata();
?>