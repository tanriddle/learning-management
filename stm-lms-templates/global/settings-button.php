<?php if(is_user_logged_in()): ?>
	<div class="stm_lms_settings_button" data-toggle="tooltip" data-placement="bottom" title="<?php esc_attr_e('Settings', 'masterstudy-lms-learning-management-system'); ?>">
		<a href="<?php echo esc_url(STM_LMS_User::user_page_url()); ?>">
			<i class="lnr lnr-user"></i>
		</a>
	</div>
<?php endif;