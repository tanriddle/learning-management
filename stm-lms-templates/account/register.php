<?php
stm_lms_register_style('register');
//enqueue_register_script();
$r_enabled = STM_LMS_Helpers::g_recaptcha_enabled();
?>

<div id="stm-lms-register">
    <h3><?php esc_html_e('Sign Up', 'masterstudy-lms-learning-management-system'); ?></h3>

    <div class="stm_lms_register_wrapper">
        <form method="post" id="st-register-form" name="st-register-form">
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="heading_font"><?php esc_html_e('Login', 'masterstudy-lms-learning-management-system'); ?></label>
						<input class="form-control"
							   type="text"
							   name="username"
							   id="st-username"
							   placeholder="<?php esc_html_e('Enter login', 'masterstudy-lms-learning-management-system'); ?>"/>
					</div>

				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="heading_font"><?php esc_html_e('E-mail', 'masterstudy-lms-learning-management-system'); ?></label>
						<input class="form-control"
							   type="email"
							   name="email"
							   id="st-email"
							   placeholder="<?php esc_html_e('Enter your E-mail', 'masterstudy-lms-learning-management-system'); ?>"/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="heading_font"><?php esc_html_e('Password', 'masterstudy-lms-learning-management-system'); ?></label>
						<input class="form-control"
							   type="password"
							   name="passwrd"
							   id="st-psw"
							   placeholder="<?php esc_html_e('Enter password', 'masterstudy-lms-learning-management-system'); ?>"/>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="heading_font"><?php esc_html_e('Password again', 'masterstudy-lms-learning-management-system'); ?></label>
						<input class="form-control"
							   type="password"
							   name="re_passwrd"
							   id="st-psw_re"
							   placeholder="<?php esc_html_e('Confirm password', 'masterstudy-lms-learning-management-system'); ?>"/>
					</div>
				</div>
			</div>
			
			
			<div class="row wuc-hidden" style="display:none;">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="heading_font"><?php esc_html_e('Degree', 'masterstudy-lms-learning-management-system'); ?></label>
                        <input class="form-control"
                               type="text"
                               name="degree"                            
                               placeholder="<?php esc_html_e('Enter Your Degree', 'masterstudy-lms-learning-management-system'); ?>"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="heading_font"><?php esc_html_e('Expertise', 'masterstudy-lms-learning-management-system'); ?></label>
                        <input class="form-control"
                               type="text"
                               name="expertize"
                               placeholder="<?php esc_html_e('Enter your Expertize', 'masterstudy-lms-learning-management-system'); ?>"/>
                    </div>
                </div>
            </div>
			<?php
			$g_recaptcha_public_key = stm_option('g_recaptcha_public_key');
			$g_recaptcha_private_key = stm_option('g_recaptcha_private_key');
				if ($g_recaptcha_public_key && $g_recaptcha_private_key):
			
				?>
			<div class="form-group">
				 <div class="g-recaptcha" data-sitekey="<?php echo $g_recaptcha_public_key; ?>"></div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<div class="stm_lms_register_wrapper__actions">

						<label class="stm_lms_styled_checkbox">
							<span class="stm_lms_styled_checkbox__inner">
								<input id="become-instructor" type="checkbox"
									   name="instructor"
									  />
								<span><i class="fa fa-check"></i> </span>
							</span>
							<span><?php esc_html_e('Register as Instructor', 'masterstudy-lms-learning-management-system'); ?></span>
						</label>
						<button type="submit" name="submit" value="Register" class="btn btn-default submit_button">
							<span><?php esc_html_e('Register', 'masterstudy-lms-learning-management-system'); ?></span>
						</button>
					</div>

				</div>
			</div>
			
		</form>

    </div>

    
	<div id="error-message">
		
	</div>
   
</div>