<?php

$r_enabled = STM_LMS_Helpers::g_recaptcha_enabled();
enqueue_login_script();
stm_lms_register_style('login');

?>

<div id="stm-lms-login" class="stm-lms-login active">

    <div class="stm-lms-login__top">
		<?php if (defined('WORDPRESS_SOCIAL_LOGIN_ABS_PATH')) do_action('wordpress_social_login'); ?>
        <h3><?php esc_html_e('Login', 'masterstudy-lms-learning-management-system'); ?></h3>
    </div>

 

	<form id="login" action="login" method="post">
	   <div class="stm_lms_login_wrapper">
			<div class="form-group">
				<label class="heading_font">
					<?php echo apply_filters('stm_lms_login_label', esc_html__('Login', 'masterstudy-lms-learning-management-system')); ?>
				</label>
				<input class="form-control"
                   type="text"
                   name="username"
                   id="username"
                   placeholder="<?php esc_html_e('Enter login', 'masterstudy-lms-learning-management-system'); ?>"/>
			</div>
			
			<div class="form-group">
            <label class="heading_font">
                <?php echo apply_filters('stm_lms_password_label', esc_html__('Password', 'masterstudy-lms-learning-management-system')); ?>
            </label>
            <input class="form-control"
                   type="password"
                   name="password"
                   id="password"
                   placeholder="<?php esc_html_e('Enter password', 'masterstudy-lms-learning-management-system'); ?>"/>
			</div>
			
			<div class="stm_lms_login_wrapper__actions">
			
				<label class="stm_lms_styled_checkbox">
					<span class="stm_lms_styled_checkbox__inner">
						<input type="checkbox" name="remember"/>
						<span><i class="fa fa-check"></i> </span>
					</span>
					<span><?php esc_html_e('Remember me', 'masterstudy-lms-learning-management-system'); ?></span>
				</label>

				<a class="lostpassword" href="<?php echo wp_lostpassword_url(); ?>" title="<?php esc_html_e('Lost Password', 'masterstudy-lms-learning-management-system'); ?>">
					<?php esc_html_e('Lost Password', 'masterstudy-lms-learning-management-system'); ?>
				</a>
				<button type="submit" name="submit" value="Login" class="btn btn-default submit_button">
					<span><?php esc_html_e('Login', 'masterstudy-lms-learning-management-system'); ?></span>
				</button>
			</div>

			<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
		</div>
		<div class="status" >
	  
		</div>
	</form>

   

</div>